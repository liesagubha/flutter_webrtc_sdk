import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_p2p_page/enter_leave_dialog.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_p2p_page/webrtc_p2p_bloc.dart';
import 'package:flutter_webrtc_sdk_example/widgets/options_dialog.dart';
import 'package:flutter_webrtc_sdk_example/widgets/player.dart';

class WebRTCPeerToPeerPage extends StatefulWidget {
  const WebRTCPeerToPeerPage({Key? key}) : super(key: key);

  @override
  _WebRTCPeerToPeerPageState createState() {
    return _WebRTCPeerToPeerPageState();
  }
}

class _WebRTCPeerToPeerPageState extends State<WebRTCPeerToPeerPage> {
  @override
  Widget build(BuildContext context) {
    final controller = context.read<P2PCallCubit>().controller;
    final controllers = context.read<P2PCallCubit>().controllers;

    return Scaffold(
      body: BlocBuilder<P2PCallCubit, CallState>(builder: (_, state) {
        final self = Stack(children: [
          Center(child: Player(controller)),
          Positioned(bottom: 0, left: 0, right: 0, child: _controls())
        ]);
        if (controllers.isNotEmpty) {
          final players = List.generate(controllers.length, (i) => Player(controllers[i]));
          final other = Row(children: players);
          return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [Expanded(flex: 7, child: self), Expanded(flex: 3, child: other)]);
        }

        return self;
      }),
    );
  }

  Widget _controls() {
    final state = context.watch<P2PCallCubit>().state;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Expanded(
          child: state is CallIdState
              ? TextFieldEx.readOnly(init: state.id, hint: 'Stream id')
              : const SizedBox(),
        ),
        StreamBuilder<WebRTCPlayerState>(
          stream: context.read<P2PCallCubit>().controller.stream,
          builder: (context, snapshot) {
            final invite = snapshot.data != WebRTCPlayerState.playing;

            return IconButton(
              icon: Icon(invite ? Icons.person_add : Icons.stop),
              constraints: const BoxConstraints(),
              onPressed: () => invite ? _invite() : _bye(),
            );
          },
        ),
        StreamBuilder<WebRTCPlayerState>(
          stream: context.read<P2PCallCubit>().controller.stream,
          builder: (context, snapshot) {
            final playing = snapshot.data == WebRTCPlayerState.playing;

            return IconButton(
              onPressed: playing ? _enterLeave : null,
              constraints: const BoxConstraints(),
              icon: const Icon(Icons.arrow_forward_sharp),
            );
          },
        ),
        IconButton(
          onPressed: () => FullscreenManager.instance.toggleFullsreen(),
          icon: const Icon(Icons.fullscreen),
        ),
      ]),
    );
  }

  void _enterLeave() async {
    final result = await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => const EnterLeaveDialog(),
    );

    if (result != null) {
      final ssid = result[1];
      if (result[0]) {
        context.read<P2PCallCubit>().getSessionInfo(ssid);
        context.read<P2PCallCubit>().enterSession(ssid);
      } else {
        context.read<P2PCallCubit>().leaveSession(ssid);
      }
    }
  }

  void _bye() {
    context.read<P2PCallCubit>().bye();
  }

  void _invite() async {
    final options = await showDialog(
      context: context,
      builder: (_) => OptionsDialog(),
    );

    if (options != null) {
      context.read<P2PCallCubit>().initialize(options[0], options[1]);
    }
  }
}
