import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class EnterLeaveDialog extends StatefulWidget {
  const EnterLeaveDialog({Key? key}) : super(key: key);

  @override
  _EnterLeaveDialogState createState() => _EnterLeaveDialogState();
}

class _EnterLeaveDialogState extends State<EnterLeaveDialog> {
  final _textController = TextEditingController(text: 'Unknown');

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Options'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [TextFieldEx(controller: _textController)],
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Cancel'),
        ),
        ElevatedButton(
          onPressed: () => Navigator.pop(context, [true, _textController.text]),
          child: const Text('Enter'),
        ),
        ElevatedButton(
          onPressed: () => Navigator.pop(context, [false, _textController.text]),
          child: const Text('Leave'),
        )
      ],
    );
  }
}
