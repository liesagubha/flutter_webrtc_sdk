import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:objectid/objectid.dart';

abstract class CallState {
  const CallState();
}

class CallIdState extends CallState {
  final String id;

  const CallIdState(this.id);
}

class CallInitState extends CallState {
  const CallInitState();
}

class CallLoadingState extends CallState {
  const CallLoadingState();
}

class CallRefreshState extends CallState {
  const CallRefreshState();
}

class CallEnterMember extends CallState {
  final String ssid;
  final String sid;

  CallEnterMember(this.ssid, this.sid);
}

class CallLeaveMember extends CallState {
  final String ssid;
  final String sid;

  CallLeaveMember(this.ssid, this.sid);
}

class P2PCallCubit extends Cubit<CallState> {
  late final controller = CallRTCPlayerController(
    onEnterClient: _enterClientCallback,
    onLeaveClient: _leaveClientCallback,
    onSessionInfo: _sessionInfoCallback,
  );

  final List<WebRTCPlayerBloc> controllers = [];

  P2PCallCubit() : super(const CallInitState()) {}

  void initialize(bool audio, bool video) async {
    final String id = ObjectId().hexString;
    emit(const CallLoadingState());
    late StreamSubscription<WebRTCPlayerState> sub;
    sub = controller.stream.listen((event) {
      if (event == WebRTCPlayerState.playing) {
        emit(CallIdState(id));
        sub.cancel();
      }
    });
    await controller.connect('wss://api.fastocloud.com', id);
    controller.initialize(audio, video, 1280, 720);
  }

  void enterSession(String ssid) {
    controller.enterSession(ssid);
  }

  void leaveSession(String ssid) {
    controller.leaveSession(ssid);
    _clean();
    emit(CallRefreshState());
  }

  void getSessionInfo(String ssid) {
    controller.getSessionInfo(ssid);
  }

  void bye() {
    controller.bye();
    _clean();
    emit(CallRefreshState());
  }

  void _clean() {
    for (int i = 0; i < controllers.length; ++i) {
      var cont = controllers[i];
      controllers.remove(cont);
      cont.dispose();
    }
  }

  void _enterClientCallback(String ssid, String sid) {
    for (int i = 0; i < controllers.length; ++i) {
      if (controllers[i].id == sid) {
        return;
      }
    }

    var rtc = PlaylistRTCPlayerController();
    rtc.connect('wss://api.fastocloud.com', sid);
    controllers.add(rtc);
    emit(CallEnterMember(ssid, sid));
  }

  void _leaveClientCallback(String ssid, String sid) {
    for (int i = 0; i < controllers.length; ++i) {
      if (controllers[i].id == sid) {
        var cont = controllers[i];
        controllers.remove(cont);
        cont.dispose();
        break;
      }
    }
    emit(CallLeaveMember(ssid, sid));
  }

  void _sessionInfoCallback(String ssid, List<String> sids) {
    for (int i = 0; i < sids.length; ++i) {
      _enterClientCallback(ssid, sids[i]);
    }
  }

  @override
  Future<void> close() async {
    controller.dispose();
    super.close();
  }
}
