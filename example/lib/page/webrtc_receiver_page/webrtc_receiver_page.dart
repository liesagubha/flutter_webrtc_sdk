import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/webrtc_receiver_bloc.dart';
import 'package:flutter_webrtc_sdk_example/widgets/buttons.dart';
import 'package:flutter_webrtc_sdk_example/widgets/hide_on_idle.dart';
import 'package:flutter_webrtc_sdk_example/widgets/player.dart';

class WebRTCReceiverPage extends StatefulWidget {
  final String? id;

  const WebRTCReceiverPage({Key? key, this.id}) : super(key: key);

  @override
  State<WebRTCReceiverPage> createState() => _WebRTCReceiverPageState();
}

class _WebRTCReceiverPageState extends State<WebRTCReceiverPage> {
  @override
  void initState() {
    if (widget.id != null) {
      context.read<PlaylistCubit>().setId(widget.id!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Center(child: Player(context.read<PlaylistCubit>().controller)),
        HideOnIdle(
          child: SizedBox.expand(
            child: Align(alignment: Alignment.bottomCenter, child: _controls(context)),
          ),
        ),
      ]),
    );
  }

  Widget _controls(BuildContext context) {
    final state = context.watch<PlaylistCubit>().state;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        const SizedBox(width: 8),
        Expanded(
          child: TextFieldEx(
            key: ValueKey<String>(state.id),
            init: state.id,
            readOnly: state is PlayerLoadingState,
            hintText: 'Stream id',
            onFieldSubmit: context.read<PlaylistCubit>().setId,
          ),
        ),
        MuteButton.icon(
          context.read<PlaylistCubit>().controller,
          mutedIcon: Icons.volume_off_rounded,
          unmutedIcon: Icons.volume_up_rounded,
        ),
        FullscreenButton(),
        IconButton(
          icon: const Icon(Icons.paste),
          onPressed: state is PlayerLoadingState
              ? null
              : () => FlutterClipboard.paste().then(context.read<PlaylistCubit>().setId),
        ),
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () => context.read<PlaylistCubit>().setId(''),
        )
      ]),
    );
  }
}
