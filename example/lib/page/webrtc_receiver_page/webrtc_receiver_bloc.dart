import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';

class PlayerIdState {
  final String id;

  const PlayerIdState(this.id);
}

class PlayerInitState extends PlayerIdState {
  const PlayerInitState() : super('');
}

class PlayerLoadingState extends PlayerIdState {
  const PlayerLoadingState(String id) : super(id);
}

class PlaylistCubit extends Cubit<PlayerIdState> {
  final controller = PlaylistRTCPlayerController();

  PlaylistCubit() : super(const PlayerInitState());

  void setId(String id) async {
    if (id.isEmpty) {
      emit(const PlayerInitState());
      controller.bye();
    } else {
      emit(PlayerLoadingState(id));
      controller.connect('wss://api.fastocloud.com', state.id);

      await controller.stream.firstWhere((e) => e == WebRTCPlayerState.playing);
      emit(PlayerIdState(id));
    }
  }

  @override
  Future<void> close() async {
    controller.dispose();
    super.close();
  }
}
