import 'package:flutter/material.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/webrtc_receiver_page.dart';

typedef Matcher = bool Function(String path);
typedef PathWidgetBuilder = Widget Function(BuildContext context, Map<String, String> args);

class Path {
  final Matcher matcher;
  final PathWidgetBuilder builder;

  const Path(this.matcher, this.builder);
}

class WebRTCReceiverRouter {
  static final _paths = [
    Path((_) => true, (_, args) => WebRTCReceiverPage(id: args['id'])),
  ];

  static Route? onGenerateRoute(RouteSettings settings) {
    if (settings.name == null) {
      return null;
    }

    for (var path in _paths) {
      final url = settings.name!;
      final parsedUri = Uri.parse(url);

      if (path.matcher(url)) {
        return PageRouteBuilder(
          pageBuilder: (ctx, _, __) => path.builder(ctx, parsedUri.queryParameters),
          transitionDuration: Duration.zero,
          settings: settings,
        );
      }
    }

    return null;
  }
}
