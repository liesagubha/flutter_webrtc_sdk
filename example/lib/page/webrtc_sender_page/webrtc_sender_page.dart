import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:flutter_webrtc_sdk_example/page/config.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_sender_page/call_cubit.dart';
import 'package:flutter_webrtc_sdk_example/widgets/hide_on_idle.dart';
import 'package:flutter_webrtc_sdk_example/widgets/options_dialog.dart';
import 'package:flutter_webrtc_sdk_example/widgets/player.dart';

class WebRTCSenderPage extends StatefulWidget {
  WebRTCSenderPage({Key? key}) : super(key: key);

  @override
  _WebRTCSenderPageState createState() => _WebRTCSenderPageState();
}

class _WebRTCSenderPageState extends State<WebRTCSenderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Center(child: Player(context.read<CallCubit>().controller)),
        HideOnIdle(
          child: SizedBox.expand(
            child: Align(alignment: Alignment.bottomCenter, child: _controls(context)),
          ),
        ),
      ]),
    );
  }

  Widget _controls(BuildContext context) {
    final state = context.watch<CallCubit>().state;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Expanded(
          child: state is CallIdState
              ? TextFieldEx.readOnly(init: state.id, hint: 'Stream id')
              : const SizedBox(),
        ),
        StreamBuilder<WebRTCPlayerState>(
          stream: context.read<CallCubit>().controller.stream,
          builder: (context, snapshot) {
            final invite = snapshot.data != WebRTCPlayerState.playing;

            return IconButton(
              icon: Icon(invite ? Icons.person_add : Icons.stop),
              constraints: const BoxConstraints(),
              onPressed: () => invite ? _invite() : _bye(),
            );
          },
        ),
        if (state is CallIdState)
          IconButton(
            icon: Icon(Icons.link),
            onPressed: () => _copyLink(state.id),
            tooltip: 'Copy link',
          ),
        IconButton(
          onPressed: () => FullscreenManager.instance.toggleFullsreen(),
          icon: const Icon(Icons.fullscreen),
        ),
      ]),
    );
  }

  void _bye() {
    context.read<CallCubit>().bye();
  }

  void _copyLink(String id) {
    final url = '$RECEIVER_URL?id=$id';

    Clipboard.setData(ClipboardData(text: url));
  }

  void _invite() async {
    final options = await showDialog(
      context: context,
      builder: (_) => OptionsDialog(),
    );

    if (options != null) {
      context.read<CallCubit>().initialize(options[0], options[1]);
    }
  }
}
