import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:objectid/objectid.dart';

abstract class CallState {
  const CallState();
}

class CallIdState extends CallState {
  final String id;

  const CallIdState(this.id);
}

class CallInitState extends CallState {
  const CallInitState();
}

class CallLoadingState extends CallState {
  const CallLoadingState();
}

class CallCubit extends Cubit<CallState> {
  final CallRTCPlayerController controller = CallRTCPlayerController();

  CallCubit() : super(const CallInitState());

  void initialize(bool audio, bool video) async {
    final String id = ObjectId().hexString;
    emit(const CallLoadingState());
    late StreamSubscription<WebRTCPlayerState> sub;
    sub = controller.stream.listen((event) {
      if (event == WebRTCPlayerState.playing) {
        emit(CallIdState(id));
        sub.cancel();
      }
    });
    await controller.connect('wss://api.fastocloud.com', id);
    controller.initialize(audio, video, 1280, 720);
  }

  void bye() {
    controller.bye();
  }

  @override
  Future<void> close() async {
    controller.dispose();
    super.close();
  }
}
