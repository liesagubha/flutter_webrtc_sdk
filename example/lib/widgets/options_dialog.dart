import 'package:flutter/material.dart';

class OptionsDialog extends StatefulWidget {
  const OptionsDialog({Key? key}) : super(key: key);

  @override
  _OptionsDialogState createState() => _OptionsDialogState();
}

class _OptionsDialogState extends State<OptionsDialog> {
  bool audio = true;
  bool video = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Options'),
      content: Column(mainAxisSize: MainAxisSize.min, children: [
        SwitchListTile(
          title: const Text('Audio'),
          value: audio,
          onChanged: (value) {
            setState(() => audio = value);
          },
        ),
        SwitchListTile(
          title: const Text('Video'),
          value: video,
          onChanged: (value) {
            setState(() => video = value);
          },
        )
      ]),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Cancel'),
        ),
        ElevatedButton(
          onPressed: !(audio || video) ? null : () => Navigator.pop(context, [audio, video]),
          child: const Text('Start'),
        )
      ],
    );
  }
}
