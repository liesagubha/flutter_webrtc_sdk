import 'package:flutter/material.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';

class Player extends StatelessWidget {
  final WebRTCPlayerBloc controller;

  const Player(this.controller);

  @override
  Widget build(BuildContext context) {
    return CustomWebRTCPlayer(
      controller: controller,
      playerBuilder: (_, __, player) => Stack(alignment: Alignment.center, children: [
        SizedBox.expand(child: player),
        if (!controller.hasVideo) Icon(Icons.videocam_off_rounded),
      ]),
      placeholder: Container(
        color: Colors.black,
        child: const Center(child: Icon(Icons.warning, color: Colors.white)),
      ),
    );
  }
}
