import 'dart:async';

import 'package:flutter/material.dart';

class HideOnIdle extends StatefulWidget {
  final Duration idleDuration;
  final Duration animationDuration;
  final bool initialValue;
  final Widget child;

  HideOnIdle({
    Key? key,
    required this.child,
    this.idleDuration = const Duration(seconds: 5),
    this.initialValue = true,
    this.animationDuration = const Duration(milliseconds: 100),
  }) : super(key: key);

  @override
  _HideOnIdleState createState() => _HideOnIdleState();
}

class _HideOnIdleState extends State<HideOnIdle> {
  late Timer _timer;
  late bool _show;

  @override
  void initState() {
    super.initState();
    _show = widget.initialValue;
    _timer = Timer(widget.idleDuration, _initTimer);
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _initTimer() {
    _timer.cancel();
    _timer = Timer(widget.idleDuration, () => setState(() => _show = false));
  }

  void _onMouseHover() {
    if (!_show) {
      setState(() => _show = true);
    }
    _initTimer();
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onHover: (_) => _onMouseHover(),
      child: AnimatedOpacity(
        opacity: _show ? 1.0 : 0.0,
        duration: widget.animationDuration,
        child: IgnorePointer(
          ignoring: !_show,
          child: widget.child,
        ),
      ),
    );
  }
}
