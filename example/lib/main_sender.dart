import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_sender_page/call_cubit.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_sender_page/webrtc_sender_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CallCubit(),
      child: MaterialApp(theme: ThemeData.dark(), home: WebRTCSenderPage()),
    );
  }
}
