import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/webrtc_receiver_router.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/webrtc_receiver_bloc.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => PlaylistCubit(),
      child: MaterialApp(
        theme: ThemeData.dark(),
        onGenerateRoute: WebRTCReceiverRouter.onGenerateRoute,
      ),
    );
  }
}
