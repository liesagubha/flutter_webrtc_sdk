import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_p2p_page/webrtc_p2p_bloc.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_p2p_page/webrtc_p2p_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => P2PCallCubit(),
      child: MaterialApp(theme: ThemeData.dark(), home: WebRTCPeerToPeerPage()),
    );
  }
}
