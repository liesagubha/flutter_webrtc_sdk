# flutter_webrtc_sdk
**Flutter WebRTC SDK for [FastoCloud](https://github.com/fastogt/fastocloud_docs/wiki) based on [Flutter WebRTC](https://github.com/flutter-webrtc/flutter-webrtc) need [Backend](https://gitlab.com/fastogt/gofastocloud_backend) can be base for Zoom or Skype like projects**

Platforms (Tested):
Android, iOS, Web

Examples:
![Zoom](https://github.com/fastogt/fastocloud_docs/raw/main/images/sm/zoom.jpg)
<img src="https://github.com/fastogt/fastocloud_docs/raw/main/images/sm/webrtc_p2p/audio_mobile.png" width="230">
<img src="https://github.com/fastogt/fastocloud_docs/raw/main/images/sm/webrtc_p2p/video_tablet.jpg" width="300">
![](https://github.com/fastogt/fastocloud_docs/raw/main/images/mixed/switcher.png)

[Monitoring panel](https://gitlab.com/fastogt/wsfastocloud)
