part of 'webrtc_player_bloc.dart';

class CallRTCPlayerController extends WebRTCPlayerBloc<SignalingCall> {
  CallRTCPlayerController({this.onEnterClient, this.onLeaveClient, this.onSessionInfo});

  EnterClientCallback? onEnterClient;
  LeaveClientCallback? onLeaveClient;
  SessionInfoCallback? onSessionInfo;

  @override
  Future<void> connect(String url, String sid, [Map<String, dynamic>? iceServers]) async {
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingCall(sid, iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.connectionClosed:
          case SignalingState.connectionError:
          case SignalingState.connectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.callStateNew:
            // renderer.srcObject = null;
            _emit(WebRTCPlayerState.loading);
            _session = session;
            break;
          case CallState.callStateBye:
            renderer.srcObject = null;
            _emit(WebRTCPlayerState.loading);
            onChangedStream?.call(null);
            _session = null;
            break;
          case CallState.callStateConnected:
            _emit(WebRTCPlayerState.playing);
            break;
          case CallState.callStateInvite:
          case CallState.callStateRinging:
        }
      }
      ..onLocalStream = ((_, stream) {
        renderer.srcObject = stream;
        _emit(WebRTCPlayerState.ready);
        onChangedStream?.call(stream);
      })
      // proxy callbacks
      ..onEnterClient = ((String ssid, String sid) {
        onEnterClient?.call(ssid, sid);
      })
      ..onLeaveClient = ((String ssid, String sid) {
        onLeaveClient?.call(ssid, sid);
      })
      ..onSessionInfo = ((String ssid, List<String> sids) {
        onSessionInfo?.call(ssid, sids);
      });

    _emit(WebRTCPlayerState.connecting);
    return _signaling!.connect('$url/webrtc_in');
  }

  void initialize(bool audio, bool video, int width, int height) {
    _signaling?.invite(audio, video, width, height);
  }

  void enterSession(String ssid) {
    _signaling?.enterSession(ssid);
  }

  void leaveSession(String ssid) {
    _signaling?.leaveSession(ssid);
  }

  void getSessionInfo(String ssid) {
    _signaling?.getSessionInfo(ssid);
  }
}
