part of 'webrtc_player_bloc.dart';

class PlaylistRTCPlayerController extends WebRTCPlayerBloc<SignalingPlaylist> {
  @override
  Future<void> connect(String url, String sid, [Map<String, dynamic>? iceServers]) async {
    _id = sid;
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingPlaylist(sid, iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.connectionClosed:
          case SignalingState.connectionError:
          case SignalingState.connectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.callStateNew:
            _emit(WebRTCPlayerState.loading);
            _session = session;
            break;
          case CallState.callStateBye:
            renderer.srcObject = null;
            _emit(WebRTCPlayerState.loading);
            onChangedStream?.call(null);
            _session = null;
            break;
          case CallState.callStateConnected:
            _emit(WebRTCPlayerState.playing);
            break;
          case CallState.callStateInvite:
          case CallState.callStateRinging:
            // TODO: ignored
            break;
        }
      }
      ..onAddRemoteStream = ((_, stream) {
        renderer.srcObject = stream;
        _emit(WebRTCPlayerState.ready);
        onChangedStream?.call(stream);
      })
      ..onRemoveRemoteStream = ((_, stream) {
        renderer.srcObject = null;
        _emit(WebRTCPlayerState.loading);
        onChangedStream?.call(null);
      });

    _emit(WebRTCPlayerState.connecting);
    return _signaling!.connect('$url/webrtc_out');
  }
}
