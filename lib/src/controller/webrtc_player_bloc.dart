import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_sdk/src/signaling/session.dart';
import 'package:flutter_webrtc_sdk/src/signaling/signaling_base.dart';
import 'package:rxdart/subjects.dart';

part 'webrtc_call_bloc.dart';
part 'webrtc_playlist_bloc.dart';

enum WebRTCPlayerState {
  initial,
  connecting,
  loading,
  ready,
  playing,
}

typedef void MediaStreamChangedCallback(MediaStream? stream);

abstract class WebRTCPlayerBloc<S extends SignalingBase> {
  final _controller = BehaviorSubject<WebRTCPlayerState>();
  final renderer = RTCVideoRenderer();

  MediaStreamChangedCallback? onChangedStream;
  S? _signaling;
  Session? _session;
  String? _id;

  Stream<WebRTCPlayerState> get stream => _controller.stream;

  MediaStream? get mediaStream => renderer.srcObject;

  String? get id => _id;

  WebRTCPlayerBloc() {
    _emit(WebRTCPlayerState.initial);
    renderer.initialize();
  }

  bool get hasAudio {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    return mediaStream!.getAudioTracks().isNotEmpty;
  }

  bool get hasVideo {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    return mediaStream!.getVideoTracks().isNotEmpty;
  }

  bool get muted {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      return !tracks[0].enabled;
    } else {
      throw 'No audio tracks';
    }
  }

  void toggleMute() {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      bool enabled = tracks[0].enabled;
      tracks[0].enabled = !enabled;
    } else {
      throw 'No audio tracks';
    }
  }

  void setMute(bool value) {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      tracks[0].enabled = value;
    } else {
      throw 'No audio tracks';
    }
  }

  void connect(String url, String sid, [Map<String, dynamic>? iceServers]);

  void bye() {
    _signaling?.bye(_session?.sid);
    _emit(WebRTCPlayerState.initial);
  }

  void dispose() {
    bye();
    _signaling?.close();
    renderer.dispose();
    _controller.close();
  }

  void _emit(WebRTCPlayerState state) {
    if (!_controller.isClosed) {
      _controller.add(state);
    }
  }
}
