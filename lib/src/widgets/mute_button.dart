import 'package:flutter/material.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';

typedef MuteButtonBuilder = Widget Function(BuildContext context, bool muted, VoidCallback? toggle);

class MuteButton extends StatefulWidget {
  final WebRTCPlayerBloc bloc;
  final MuteButtonBuilder builder;

  const MuteButton({
    Key? key,
    required this.bloc,
    required this.builder,
  }) : super(key: key);

  factory MuteButton.icon(
    WebRTCPlayerBloc bloc, {
    IconData mutedIcon = Icons.mic_off_rounded,
    IconData unmutedIcon = Icons.mic_rounded,
  }) {
    return MuteButton(
      bloc: bloc,
      builder: (_, muted, toggle) => IconButton(
        onPressed: toggle,
        icon: Icon(muted ? mutedIcon : unmutedIcon),
        constraints: const BoxConstraints(),
      ),
    );
  }

  @override
  _MuteButtonState createState() => _MuteButtonState();
}

class _MuteButtonState extends State<MuteButton> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<WebRTCPlayerState>(
      stream: widget.bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.data == WebRTCPlayerState.playing) {
          final muted = !widget.bloc.hasAudio || widget.bloc.muted;

          return widget.builder(context, muted, _toggle);
        }
        return const SizedBox();
      },
    );
  }

  void _toggle() {
    if (widget.bloc.hasAudio) {
      widget.bloc.toggleMute();
      setState(() {});
    }
  }
}
