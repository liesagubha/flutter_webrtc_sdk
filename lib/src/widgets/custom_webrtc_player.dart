import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_sdk/src/controller/webrtc_player_bloc.dart';

typedef PlayerBuilder = Widget Function(
    BuildContext context, WebRTCPlayerBloc controller, Widget player);

class CustomWebRTCPlayer extends StatelessWidget {
  static const _connetingFallback = Center(child: CircularProgressIndicator(color: Colors.red));
  static const _loadingFallback = Center(child: CircularProgressIndicator(color: Colors.yellow));
  static const _readyFallback = Center(child: CircularProgressIndicator(color: Colors.green));
  static const _placeholderFallback = SizedBox();

  final WebRTCPlayerBloc controller;
  final PlayerBuilder playerBuilder;
  final double aspectRatio;
  final Widget connecting, loading, ready, placeholder;

  const CustomWebRTCPlayer({
    Key? key,
    required this.controller,
    required this.playerBuilder,
    this.aspectRatio = 16 / 9,
    this.connecting = _connetingFallback,
    this.loading = _loadingFallback,
    this.ready = _readyFallback,
    this.placeholder = _placeholderFallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: Container(
        color: Colors.black,
        child: StreamBuilder<WebRTCPlayerState>(
          stream: controller.stream,
          builder: (ctx, snapshot) => _player(ctx, snapshot.data),
        ),
      ),
    );
  }

  Widget _player(BuildContext context, WebRTCPlayerState? state) {
    switch (state) {
      case WebRTCPlayerState.playing:
        final player = RTCVideoView(controller.renderer);
        return playerBuilder(context, controller, player);
      case WebRTCPlayerState.ready:
        return ready;
      case WebRTCPlayerState.loading:
        return loading;
      case WebRTCPlayerState.connecting:
        return connecting;
      default:
        return placeholder;
    }
  }
}
