// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

class DeviceInfo {
  static String get label {
    return 'FastoCloud ( ' + html.window.navigator.userAgent + ' )';
  }

  static String get userAgent {
    return 'fastocloud/web-plugin 0.0.1';
  }
}
