part of signaling;

const String kEnterCommand = 'enter';
const String kLeaveCommand = 'leave';
const String kSessionInfoCommand = 'session_info';

const String kTrack = 'track';
const String kSession = 'session';
const String kTracks = 'tracks';

typedef void EnterClientCallback(String ssid, String sid);
typedef void LeaveClientCallback(String ssid, String sid);
typedef void SessionInfoCallback(String ssid, List<String> sids);

class SignalingCall extends SignalingBase {
  SignalingCall(String sid, [Map<String, dynamic>? iceServers]) : super(sid, iceServers);

  late MediaStream _localStream;

  StreamStateCallback? onLocalStream;

  EnterClientCallback? onEnterClient;
  LeaveClientCallback? onLeaveClient;
  SessionInfoCallback? onSessionInfo;

  final Map<String, dynamic> _dcConstraints = {
    'mandatory': {},
    'optional': [],
  };

  void invite(bool audio, bool video, int width, int height) async {
    assert(audio || video);
    var sessionId = streamId;
    var session = Session(sid: sessionId, peerId: _selfId);
    final Map<String, dynamic> mediaConstraints = {};
    if (audio) {
      mediaConstraints['audio'] = true;
    }
    if (video) {
      mediaConstraints['video'] = {
        'mandatory': {'minWidth': width, 'minHeight': height},
        'facingMode': 'user',
        'optional': []
      };
    }

    _localStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
    onLocalStream?.call(session, _localStream);
    _sessions[sessionId] = session;

    final pc = await _initSession(session, _dcConstraints);
    session.pc = pc;

    _createOffer(pc);
    onCallStateChange?.call(session, CallState.callStateNew);
  }

  @override
  void onMessage(message) async {
    Map<String, dynamic> mapData = message;
    var data = mapData['data'];
    var type = mapData['type'];

    switch (type) {
      case kNewCommand:
        {
          var peerId = data[kTo]; // peer
          var streamId = data[kFrom]; // streamid
          var sessionId = data[kSessionID]; // sessionid
        }
        break;
      case kAnswerCommand:
        {
          var peerId = data[kTo]; // peer
          var streamId = data[kFrom]; // streamid
          var sessionId = data[kSessionID]; // sessionid
          var description = data['description']; // sdp generated by gstreamer

          var session = _sessions[sessionId];
          if (session == null) {
            return;
          }

          await session.pc?.setRemoteDescription(
              RTCSessionDescription(description['sdp'], description['type']));
        }
        break;
      case kCandidateCommand:
        {
          var peerId = data[kTo]; // peer
          var streamId = data[kFrom]; // streamid
          var sessionId = data[kSessionID]; // sessionid
          var candidateMap = data['candidate'];

          Session? session = _sessions[sessionId];
          if (session == null) {
            return;
          }

          String? candidateField = candidateMap['candidate'];
          String? sdpMidField = candidateMap['sdpMid'];
          int? mlindexField = candidateMap['sdpMLineIndex'];

          final RTCIceCandidate candidate =
              RTCIceCandidate(candidateField, sdpMidField, mlindexField);
          session.pc?.addCandidate(candidate);
        }
        break;
      case kEnterCommand:
        {
          var from = data[kFrom];
          var track = data[kSessionID];
          onEnterClient?.call(from, track);
        }
        break;
      case kLeaveCommand:
        {
          var from = data[kFrom];
          var track = data[kSessionID];
          onLeaveClient?.call(from, track);
        }
        break;
      case kSessionInfoCommand:
        {
          var from = data[kFrom];
          final List<String> tracks = List<String>.from(data[kTracks]);
          onSessionInfo?.call(from, tracks);
        }
        break;
      case kByeCommand:
        {
          var peerId = data[kTo]; // peer
          var sessionId = data[kSessionID]; // sessionid

          var session = _sessions.remove(sessionId);
          onCallStateChange?.call(session, CallState.callStateBye);
          _disposeLocalStream();
          session?.close();
        }
        break;
      default:
        break;
    }
  }

  @override
  void onSdpSemantics(_, RTCPeerConnection pc) async {
    switch (sdpSemantics) {
      case 'plan-b':
        pc.addStream(_localStream);
        break;
      case 'unified-plan':
        for (var track in _localStream.getTracks()) {
          pc.addTrack(track, _localStream);
        }

        final audioTracks = _localStream.getAudioTracks();
        if (audioTracks.isNotEmpty) {
          final init = RTCRtpTransceiverInit(
              direction: TransceiverDirection.SendOnly, streams: [_localStream]);
          RTCRtpTransceiver trans = await pc.addTransceiver(track: audioTracks[0], init: init);
        }

        final videoTracks = _localStream.getVideoTracks();
        if (videoTracks.isNotEmpty) {
          final init = RTCRtpTransceiverInit(
              direction: TransceiverDirection.SendOnly, streams: [_localStream]);
          RTCRtpTransceiver trans = await pc.addTransceiver(track: videoTracks[0], init: init);
        }
        break;
    }
  }

  void enterSession(String ssid) {
    _send(kEnterCommand, {kFrom: _selfId, kTrack: streamId, kSession: ssid});
  }

  void leaveSession(String ssid) {
    _send(kLeaveCommand, {kFrom: _selfId, kTrack: streamId, kSession: ssid});
  }

  void getSessionInfo(String ssid) {
    _send(kSessionInfoCommand, {kFrom: _selfId, kTrack: streamId, kSession: ssid});
  }

  @override
  void bye(String? sessionId) {
    super.bye(sessionId);
    _disposeLocalStream();
  }

  void _createOffer(RTCPeerConnection peer) {
    peer.createOffer(_dcConstraints).then((s) {
      peer.setLocalDescription(s);
      _send(kOfferCommand, {
        kFrom: _selfId,
        kTo: streamId,
        'description': s.sdp,
        'type': s.type,
      });
    });
  }

  Future<void> _disposeLocalStream() async {
    _localStream.getTracks().forEach((element) async {
      await element.stop();
    });
    await _localStream.dispose();
  }
}
