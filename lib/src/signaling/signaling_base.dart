library signaling;

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/foundation.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:random_string/random_string.dart';

import '../utils/device_info.dart' if (dart.library.js) '../utils/device_info_web.dart';
import '../websocket/websocket.dart';
import 'session.dart';

part 'signaling_call.dart';
part 'signaling_playlist.dart';

const String kFrom = 'from';
const String kTo = 'to';
const String kSessionID = 'session_id';

// comands
const String kNewCommand = 'new';
const String kOfferCommand = 'offer';
const String kAnswerCommand = 'answer';
const String kCandidateCommand = 'candidate';
const String kByeCommand = 'bye';

enum SignalingState {
  connectionOpen,
  connectionClosed,
  connectionError,
}

enum CallState {
  callStateNew,
  callStateRinging,
  callStateInvite,
  callStateConnected,
  callStateBye,
}

/*
 * callbacks for Signaling API.
 */
typedef void SignalingStateCallback(SignalingState state);
typedef void CallStateCallback(Session? session, CallState state);
typedef void StreamStateCallback(Session session, MediaStream stream);
typedef void OtherEventCallback(dynamic event);
typedef void LogsEventCallback(String event);

abstract class SignalingBase {
  static const _iceServers = <String, dynamic>{
    'iceServers': [
      {'urls': 'stun:stun.fastocloud.com:3478'},
      {
        'urls': 'turn:turn.fastocloud.com:5349',
        'credential': 'fastocloud',
        'username': 'fastocloud'
      },
    ]
  };

  late final WebSocketDelegate _socket;
  final Map<String, Session> _sessions = {};
  final String _selfId = randomNumeric(8);

  final String streamId;
  final Map<String, dynamic> iceServers;

  SignalingStateCallback? onSignalingStateChange;
  CallStateCallback? onCallStateChange;

  String get sdpSemantics => WebRTC.platformIsWindows ? 'plan-b' : 'unified-plan';

  SignalingBase(this.streamId, [Map<String, dynamic>? iceServers])
      : iceServers = iceServers ?? _iceServers;

  @protected
  void onMessage(message);

  @protected
  void onSdpSemantics(Session session, RTCPeerConnection pc);

  Future<void> connect(String url) async {
    _socket = WebSocketDelegate(url, onMessage: _onMessage, onError: _onError, onClose: _onClose);

    await _socket.connect();

    onSignalingStateChange?.call(SignalingState.connectionOpen);

    _send(kNewCommand, {
      kFrom: _selfId,
      kTo: streamId,
      'name': DeviceInfo.label,
      'user_agent': DeviceInfo.userAgent,
    });
  }

  void _onMessage(dynamic message) {
    final decoded = jsonDecode(message);
    developer.log('[RECEIVE]: $message');
    onMessage(decoded);
  }

  void _onError(dynamic error) {
    developer.log('Error: ' + error);
    onSignalingStateChange?.call(SignalingState.connectionError);
  }

  void _onClose(int? code, String? reason) {
    developer.log('Closed by server [$code => $reason]!');
    onSignalingStateChange?.call(SignalingState.connectionClosed);
  }

  void bye(String? sessionId) {
    if (sessionId == null) {
      return;
    }

    _send(kByeCommand, {kFrom: _selfId, kTo: sessionId});
    _sessions[sessionId]?.close();
  }

  void close() async {
    for (var session in _sessions.values) {
      await session.close();
    }

    _sessions.clear();
    _socket.close();
  }

  void _send(event, data) {
    final request = {
      'type': event,
      'data': data,
    };

    _socket.send(jsonEncode(request));
    developer.log('[SEND]: $event: $data');
  }

  Future<RTCPeerConnection> _initSession(Session session, Map<String, dynamic> config) async {
    final args = <String, dynamic>{...iceServers, 'sdpSemantics': sdpSemantics};
    developer.log('initSession: ' + args.toString());
    RTCPeerConnection pc = await createPeerConnection(args, config);

    onSdpSemantics(session, pc);

    pc.onIceCandidate = (RTCIceCandidate candidate) {
      _send(kCandidateCommand, {
        kFrom: _selfId,
        kTo: streamId,
        'sdpMLineIndex': candidate.sdpMLineIndex,
        'sdpMid': candidate.sdpMid,
        'candidate': candidate.candidate
      });
    };

    pc.onIceConnectionState = (state) {
      developer.log('onIceConnectionState: ' + state.toString());
      if (state == RTCIceConnectionState.RTCIceConnectionStateConnected) {
        onCallStateChange?.call(session, CallState.callStateConnected);
      }
    };

    return pc;
  }
}
