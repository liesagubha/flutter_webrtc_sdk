import 'package:flutter_webrtc/flutter_webrtc.dart';

class Session {
  final String peerId;
  final String sid;
  final List<RTCIceCandidate> remoteCandidates = [];

  RTCPeerConnection? pc;

  Session({required this.sid, required this.peerId});

  Future<void> initialize(Map<String, dynamic> configuration,
      [Map<String, dynamic> constraints = const {}]) async {
    pc = await createPeerConnection(configuration, constraints);
  }

  Future<void> close() async {
    await pc?.close();
  }
}
