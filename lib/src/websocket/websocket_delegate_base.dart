typedef void OnMessageCallback(dynamic msg);
typedef void OnCloseCallback(int? code, String? reason);
typedef void OnOpenCallback();
typedef void OnErrorCallback(String error);

abstract class WebSocketDelegateBase {
  final String url;
  final OnMessageCallback? onMessage;
  final OnCloseCallback? onClose;
  final OnErrorCallback? onError;

  WebSocketDelegateBase(this.url, this.onMessage, this.onClose, this.onError);

  Future<void> connect();

  void send(dynamic data);

  void close();
}
