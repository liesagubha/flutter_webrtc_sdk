import 'dart:io';

import 'websocket_delegate_base.dart';

class WebSocketDelegate extends WebSocketDelegateBase {
  late final WebSocket _socket;

  WebSocketDelegate(
    String url, {
    OnMessageCallback? onMessage,
    OnCloseCallback? onClose,
    OnErrorCallback? onError,
  }) : super(url, onMessage, onClose, onError);

  @override
  Future<void> connect() async {
    try {
      _socket = await WebSocket.connect(url);

      _socket.listen(
        onMessage,
        onDone: () => onClose?.call(_socket.closeCode, _socket.closeReason),
        onError: onError,
      );
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  @override
  void send(data) {
    _socket.add(data);
  }

  @override
  void close() {
    _socket.close();
  }
}
