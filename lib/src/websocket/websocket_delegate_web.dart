// ignore: avoid_web_libraries_in_flutter
import 'dart:async';
import 'dart:html';

import 'websocket_delegate_base.dart';

class WebSocketDelegate extends WebSocketDelegateBase {
  late final WebSocket _socket;

  WebSocketDelegate(
    String url, {
    OnMessageCallback? onMessage,
    OnCloseCallback? onClose,
    OnErrorCallback? onError,
  }) : super(url, onMessage, onClose, onError);

  @override
  Future<void> connect() async {
    try {
      final openedCompleter = Completer();

      _socket = WebSocket(url);
      _socket.onOpen.listen((_) => openedCompleter.complete());
      _socket.onMessage.listen((e) => onMessage?.call(e.data));
      // TODO(?) e doesn't contain data field
      _socket.onError.listen((e) => onError?.call(e.toString()));
      _socket.onClose.listen((e) => onClose?.call(e.code, e.reason));

      await openedCompleter.future;
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  @override
  void send(data) {
    _socket.send(data);
  }

  @override
  void close() {
    _socket.close();
  }
}
